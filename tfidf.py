import math
import operator

import ngrams
import stopwords
import tokenizer

import jaccard

def words_list(docs):
    res = set()
    for x in docs:
        res |= set(x)
    return res
    


class TfIdf:

    def __init__(self, docs):
        self.docs = docs
        self.swords = words_list(self.docs)
        self.lwords = list(self.swords)

        self.tf = [[self.get_tf(wid, tid) for tid in range(len(self.docs))] for wid in range(len(self.lwords))]
        self.idf = [self.get_idf(wid) for wid in range(len(self.lwords))]
        
        self.w = [[self.tf[wid][tid] * self.idf[wid] for tid in range(len(self.docs))] for wid in range(len(self.lwords))]


    def get_tf(self, wid, tid):
        word = self.lwords[wid]
        doc = self.docs[tid]
        return doc.count(word) / len(doc)

    def get_idf(self, wid):
        word = self.lwords[wid]
        df = 0
        for doc in self.docs:
            if word in doc:
                df += 1
        if df <= 4: # Remove proper nouns
            return 0
        return math.log10(len(self.docs) / df)


    def get_n_best(self, n):

        vals = []
        for wid in range(len(self.lwords)):
            wval = max(self.w[wid])
            vals.append((self.lwords[wid], wval))


        vals = sorted(vals, key=operator.itemgetter(1), reverse=True)
        vals = vals[:n]
        return [x[0] for x in vals]
        
    

def prepocess(doc):
    doc = tokenizer.tokenize(doc)
    doc = stopwords.clean_stopwords(doc)
    doc = ngrams.get_ngrams(doc, 1)
    print(doc)
    return doc


if __name__ == '__main__':
    
    docs = [
        'The car is driven on the road.',
        'The truck is driven on the highway'
    ]

    docs = [prepocess(x) for x in docs]

    tf_idf = TfIdf(docs)
    words = tf_idf.get_n_best(5)
    
    print(words)
    
    print(jaccard.jaccard_sim(words, words))
    print(jaccard.jaccard_dist(words, words))
