def jaccard_sim(a, b):
    return len(set(a) & set(b)) / len(set(a) | set(b))

def jaccard_dist(a, b):
    return 1 - jaccard_sim(a, b)
