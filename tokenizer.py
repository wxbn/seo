

def tokenize(s):

    rem = '.,;:!?\'"#~-_()[]{}'

    def parse_char(c):
        if c in rem:
            return ' '
        else:
            return c
    
    s = s.lower()
    s = ''.join([parse_char(c) for c in s])
    tokens = s.split()
    return tokens
