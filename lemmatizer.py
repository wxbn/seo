class Lemmatizer:
    def __init__(self, filename):
        self.lemmes = {}
        self.load_lemmes(filename)

    def load_lemmes(self, filename):
        with open(filename) as f:
            lines = f.readlines()

        for line in lines:
            split = line.strip().split('\t')
            self.lemmes[split[1]] = split[0]

    def lemmatize(self, doc):
        for i in range(len(doc)):
            doc[i] = doc[i].replace(".","").replace(",","")
            if doc[i] in self.lemmes:
                doc[i] = self.lemmes[doc[i]]
        return doc

if __name__ == '__main__':
    lemmatizer = Lemmatizer("lemmatization-en.txt")
    print(lemmatizer.lemmatize("The purpose of both stemming and lemmatization is to reduce morphological variation. This is in contrast to the the more general term conflation procedures, which may also address lexico-semantic, syntactic, or orthographic variations.".split(' ')))