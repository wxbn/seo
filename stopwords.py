

_words = None
with open('stopwords.data', 'r') as f:
    _words = set([l.strip() for l in f.readlines()])


def clean_stopwords(tokens):
    res = []
    for x in tokens:
        if x not in _words:
            res.append(x)
    return res
