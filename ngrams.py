def get_ngrams(words, n):
    if n == 1:
        return words

    res = []
    nres = (len(words) + n - 1) // n
    for i in range(nres):
        res.append((' ').join(words[i*n:(i+1)*n]))
    return res

