import jaccard
import ngrams
import stopwords
import tfidf
import tokenizer
import lemmatizer

def prepocess(doc):
    doc = tokenizer.tokenize(doc)
    doc = stopwords.clean_stopwords(doc)
    lemma = lemmatizer.Lemmatizer("lemmatization-en.txt")
    doc = lemma.lemmatize(doc)
    doc = ngrams.get_ngrams(doc, 2)
    return doc

def analyze_ti(docs):
    tf_idf = tfidf.TfIdf(docs)
    words = tf_idf.get_n_best(100)
    return words

class TextClassifier:

    def __init__(self, preprocess_fn, extract_fn, dist_fn):

        self.preprocess_fn = preprocess_fn
        self.extract_fn = extract_fn
        self.dist_fn = dist_fn

    def fit(self, data):

        X = [self.preprocess_fn(doc[0]) for doc in data]
        y = [doc[1] for doc in data]
        
        self.data = data

        self.extracts = dict()
        labels = set(y)
        for label in labels:
            docs = [X[i] for i in range(len(y)) if y[i] == label]
            self.extracts[label] = self.extract_fn(docs)

    def predict(self, doc):

        docs = [self.preprocess_fn(doc)]
        vals = self.extract_fn(docs)
        
        min_dist = float('+inf')
        min_label = None
        for l in self.extracts:
            dist = self.dist_fn(vals, self.extracts[l])
            if dist < min_dist:
                min_dist = dist
                min_label = l

        return min_label

    def manual_clean(self):
        remove = print("Do you want to remove some n-grams ? [y/n]")
        if remove == "n":
            return
        for label in self.extracts:
            print(label + " : ")
            res = []
            for ngram in self.extracts[label]:
                question = "\tDo you want to remove \"" + ngram + "\" ? [y/n] (default no)"
                answer = input(question)
                if answer != "y":
                    res.append(ngram)
            self.extracts[label] = res

    def test(self, data):

        succ = 0
        for x in data:
            pred = self.predict(x[0])
            if pred == x[1]:
                succ += 1

        perc = succ * 100 / len(data)
        print('Accurary: {}/{} ({}%)'.format(succ, len(data), perc))



if __name__ == '__main__':


    train_set = [
        ('Cuda is the best programming language', 'it'),
        ('The car is driven on the road.', 'car'),
        ('The truck is driven on the highway', 'car'),
        ('I am an expert in language programming', 'it')
    ]

    test_set = [
        ('I love car', 'car'),
        ('Cuda is god', 'it')
    ]

    model = TextClassifier(prepocess, analyze_ti, jaccard.jaccard_dist)
    model.fit(train_set)
    model.test(test_set)
    
