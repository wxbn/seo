import os
import time
import jaccard
import model
from   model            import TextClassifier
import pyforms
from   pyforms          import BaseWidget
from   pyforms.controls import ControlText
from   pyforms.controls import ControlButton
from   pyforms.controls import ControlDir
from   pyforms.controls import ControlTextArea
from   pyforms.controls import ControlLabel
from   pyforms.controls import ControlFile
from   pyforms.controls import ControlCheckBoxList

def label_to_id(themes, label):
    return themes.index(label)

def id_to_label(themes, ID):
    return themes[ID]

class App(BaseWidget):

    def __init__(self):
        super(App, self).__init__('Search Engine Optimization Project - Theming')

        #Definition of the forms fields TRAINING
        self._themename     = ControlText('Theme name')
        self._directory     = ControlDir('Select a directory')
        self._themelist     = ControlTextArea('List of themes')
        self._ngramslist    = ControlCheckBoxList('Select n-grams')
        self._commitbutton  = ControlButton('Commit this theme')
        self._clearbutton   = ControlButton('Clear themes')
        self._trainbutton   = ControlButton('TRAIN')
        self._ngramsbutton  = ControlButton('Select ngrams')
        #Definition of the forms fields RUNNING
        self._file          = ControlFile('Select a text file')
        self._prediction    = ControlLabel('Prediction:')
        self._runbutton     = ControlButton('PREDICT')
        #Definition of the forms fields CONSOLE OUT ERRORS
        self._outerr = ControlTextArea('Console info')

        #Define the button action
        self._commitbutton.value = self.__commitbuttonAction
        self._clearbutton.value = self.__clearbuttonAction
        self._trainbutton.value = self.__trainbuttonAction
        self._runbutton.value = self.__runbuttonAction
        self._ngramsbutton.value = self.__ngramsbuttonAction

        #Attributes
        self.model = TextClassifier(model.prepocess, model.analyze_ti, jaccard.jaccard_dist)
        self.fileio = FileIO(self._outerr)
        self.theme_paths = {}
        self.paths = []
        self.themes = []
        self.trained = False

        #Disable text area user input
        self._themelist.autoscroll = True
        self._themelist.readonly = True
        self._outerr.autoscroll = True
        self._outerr.readonly = True

        #Space displaying
        self.formset = [ {
            '1 #Train': [('_themename', ' '), ('_directory', ' '), ('_commitbutton', '_clearbutton', ' ',), ' ',
                         ('_themelist', '_ngramslist'), ('_trainbutton', ' ', '_ngramsbutton'), ' '],
            '2 #Run'  : [('_file', ' '), ' ', ('_runbutton', '_prediction', ' ') , ' ']
        },
            '=', (' ', '_outerr', ' ') ]
        #Use dictionaries for tabs
        #Use the sign '=' for a vertical splitter
        #Use the signs '||' for a horizontal splitter

    def __commitbuttonAction(self):
        """Button action event"""
        if (self._themename.value != '' and self._directory.value != ''):
            if (self._themename.value not in self.theme_paths):
                self.theme_paths[self._themename.value] = set()
            self.paths.append(self._directory.value)
            self.themes.append(self._themename.value)
            self.theme_paths[self._themename.value].add(self._directory.value)
            self._themelist.value = ''
            self.map_theme_paths(self.display_theme_paths)


    def __clearbuttonAction(self):
        """Button action event"""
        if not (self.themes):
            self.fileio.output('No themes to clear')
        else:
            if self.trained:
                self.model.extracts = dict()
            self.trained = False
            self.theme_paths = {}
            self.paths = []
            self.themes = []
            self._ngramslist.clear()
            self._themelist.value = ''
            self.fileio.output('Clear themes', 'INFO')

    def __trainbuttonAction(self):
        """Button action event"""
        if (len(self.paths) > 0):
            self.fileio.output('Training ...', 'INFO')
            docs = self.fileio.map_to_docs(self.theme_paths, self.themes)
            self._ngramslist.clear()
            self.model.fit(docs)
            for label in self.model.extracts:
                for ngram in self.model.extracts[label]:
                    self._ngramslist += ('[' + id_to_label(self.themes, label) + '] -> ' + ngram, True)
            self.trained = True
            self.fileio.output('Model trained for ' + str(self.themes) + ' themes', 'INFO')

    def __ngramsbuttonAction(self):
        """Button action event"""
        if not(self.trained):
            self.fileio.output('Trained the model before')
        else:
            selectedNgrams = dict()
            ngrams = self._ngramslist.value
            for ngram in ngrams:
                label = ngram[1:ngram.index(']')]
                gram = ngram[ngram.index('>') + 2:]
                if not (label in selectedNgrams):
                    selectedNgrams[label] = []
                selectedNgrams[label].append(gram)
            len_before = sum([len(self.model.extracts[k]) for k in self.model.extracts])
            len_after = sum([len(selectedNgrams[k]) for k in selectedNgrams])
            self.fileio.output(str(len_after) + '/' + str(len_before) + ' selected ngrams', 'INFO')
            self.model.extracts = selectedNgrams

    def __runbuttonAction(self):
        """Button action event"""
        if not(self.trained):
            self.fileio.output('Trained the model before')
        elif (self._file.value != ''):
            self.fileio.output('Predicting ...', 'INFO')
            doc = self.fileio.file_to_doc(self._file.value)
            label = self.model.predict(doc[0])
            self._prediction.value = 'Theme prediction: ' + str(label)
            self.fileio.output(self._prediction.value, 'INFO')

    def map_theme_paths(self, process):
        for theme in self.theme_paths:
            process(theme)

    def display_theme_paths(self, theme):
        self._themelist.value += theme + ' => ' + str(self.theme_paths[theme]) + '\n'

class FileIO:

    def __init__(self, outerr):
        self._outerr = outerr

    def map_to_docs(self, theme_paths, themes):
        docs = []
        for theme in theme_paths:
            for path in theme_paths[theme]:
                docs = docs + self.dir_to_docs(path, label_to_id(themes, theme))
        return docs

    def dir_to_docs(self, path, theme):
        docs = []
        if not (os.path.isdir(path)):
            self.output(path + ': not a directory\n')
            return docs
        else:
            for filePath in os.listdir(path):
                docs.append(self.file_to_doc(path + '/' + filePath, theme))
        return docs

    def file_to_doc(self, path, theme=" "):
        if not (os.path.isfile(path)):
            self.output(path + ': not a file\n')
            return ''
        else:
            with open(path, 'rb') as myfile:
                return (myfile.read().decode("windows-1252").strip().replace("\n", " "), theme)

    def output(self, msg, level='ERROR'):
        self._outerr.__add__('[' + level +'] ' + msg)


#Execute the application
if __name__ == "__main__":   pyforms.start_app(App)
