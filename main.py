import os
import jaccard
import model


print("#############################################")
print("#############THEME CLASSIFIER################")
print("#############################################")

themes = []
paths = []

while True:
    theme = input("Please type a theme : ")
    themes.append(theme)

    path = ""
    while not(os.path.isdir(path)):
        path = input("Please type a path to directory containing corresponding texts : ")
    paths.append(path)

    answer = ""
    while answer != 'y' and answer != 'n':
        answer = input("Would you like to add another theme ? [y/n]")
    if answer == "n":
        break

textPath = ""
while not(os.path.isfile(textPath)):
    textPath = input("Please type a path to a text to classify : ")

print("Loading dataset")

train_set = []
for i in range(len(themes)):
    for filePath in os.listdir(paths[i]):
        with open(paths[i] + "/" + filePath, 'rb') as myfile:
            train_set.append((myfile.read().decode("windows-1252").strip().replace("\n", " "), themes[i]))

print("Training model (may take several minutes)")

model = model.TextClassifier(model.prepocess, model.analyze_ti, jaccard.jaccard_dist)
model.fit(train_set)
model.manual_clean()

"""
good = 0
overall = 0
for i in range(len(themes)):
    for filePath in os.listdir(paths[i]):
        with open(paths[i] + "/" + filePath, 'rb') as myfile:
            if model.predict(myfile.read().decode("windows-1252").strip().replace("\n", " ")) == themes[i]:
                good += 1
            overall += 1

print("Accuraccy : " + str(good/overall))
"""


with open(textPath, 'rb') as myfile:
    print(model.predict(myfile.read().decode("windows-1252").strip().replace("\n", " ")))